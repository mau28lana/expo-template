import React, { useState, useEffect } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { Text, View} from '../../components/Themed';
import { AuthScreenProps } from '../../types';
import AuthForm from './AuthComponent';

export default function LoginScreen({navigation}: AuthScreenProps<'Login'>) {

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Masuk</Text>
      <Text style={styles.welcomeMessege}>Selamat datang lagi di Fallet!</Text>
      <AuthForm 
        isRegister={false} 
        buttonText={"Masuk"}
        buttonOnPress={()=>navigation.navigate("Register")}
        question={"Tidak punya akun?"}
        questionButton={"Buat akun"}
        questionOnPress={()=>navigation.navigate("Register")}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 32,
    paddingStart: 32
  },
  welcomeMessege: {
    fontSize: 20,
    fontWeight: '300',
    paddingBottom: 100,
    paddingStart: 32
  },
});
