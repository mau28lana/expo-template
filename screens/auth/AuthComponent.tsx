import { Entypo, Ionicons, MaterialIcons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import { useEffect, useState } from 'react';
import { Button, Dimensions, Pressable, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Text, View } from '../../components/Themed';
import Layout from '../../constants/Layout';


export default function AuthForm({
    isRegister, 
    buttonText,
    buttonOnPress,
    question,
    questionButton,
    questionOnPress,
  }:{
      isRegister: boolean,
      buttonText: String,
      buttonOnPress: any,
      question: String,
      questionButton: any,
      questionOnPress: any
    }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confPassword, setConfPassword] = useState('');
    const [screenWidth, setScreenWidth] = useState(Dimensions.get('window').width);
    
    
    const [secureTextEntry, setSecureTextEntry] = useState(true);
    const [secureTextEntry1, setSecureTextEntry1] = useState(true);
  
    const toggleSecureTextEntry = () => {
        setSecureTextEntry(!secureTextEntry);
    };
    
    const toggleSecureTextEntry1 = () => {
        setSecureTextEntry1(!secureTextEntry);
    };
  
    useEffect(() => {
      const onChange = (result: { window: { width: React.SetStateAction<number>; }; }) => {
        setScreenWidth(result.window.width);
      };
  
      Dimensions.addEventListener('change', onChange);
  
      return () => {
        Dimensions.addEventListener('change', onChange);
      };
    });
    return (
        <View style={[styles.formContainer]}>
            <View style={styles.inputContainer}>
                <MaterialIcons name="email" size={20} color="#eee" />
                <TextInput
                    style={[styles.input]}
                    placeholder="Email"
                    onChangeText={text => setEmail(text)}
                    value={email}
                />
            </View>
            <View style={styles.inputContainer}>
                <Entypo name='lock' size={20} color="#eee" />
                <TextInput
                    style={[styles.input]}
                    placeholder="Password"
                    onChangeText={text => setPassword(text)}
                    value={password}
                    secureTextEntry={secureTextEntry}
                />
                <TouchableOpacity onPress={toggleSecureTextEntry}>
                    <Ionicons
                    name={!secureTextEntry ? 'ios-eye-off' : 'ios-eye'}
                    size={24}
                    color="#e7e7e7"
                    />
                </TouchableOpacity>
            </View>
            {isRegister 
            ?   <View style={styles.inputContainer}>
                    <Entypo name='lock' size={20} color="#eee" />
                    <TextInput
                        style={[styles.input]}
                        placeholder="Re-Type Password"
                        onChangeText={text => setConfPassword(text)}
                        value={confPassword}
                        secureTextEntry={secureTextEntry1}
                    />
                    <TouchableOpacity onPress={toggleSecureTextEntry1}>
                        <Ionicons
                        name={!secureTextEntry1 ? 'ios-eye-off' : 'ios-eye'}
                        size={24}
                        color="#e7e7e7"
                        />
                    </TouchableOpacity>
                </View>
            :   null
            }

            {isRegister
            ? null
            : <Text style={styles.lupaPassword}>Lupa kata sandi?</Text>
            }

            <AuthButton 
                buttonText={buttonText}
                buttonOnPress={buttonOnPress}
            />
            
            <View style={styles.questionContainer}>
              <Text style={styles.qeustion}>
                {question}
              </Text>
              <TouchableOpacity onPress={questionOnPress}>
                <Text style={styles.qeustionButton}>{questionButton}</Text>
              </TouchableOpacity>
            </View>
            
            <View style={styles.collumn}>
              <Text>
                Dengan melanjutkan kamu menyetujui
              </Text>
              <View style={styles.row}>
                <Text style={styles.qeustionButton}>
                  Persyaratan Layanan
                </Text>
                <Text> dan</Text>
                <Text style={styles.qeustionButton}>
                  Kebijakan Privasi
                </Text>
              </View>
            </View>

        </View>
  );
}

function AuthButton({
        buttonText,
        buttonOnPress
    }:{
        buttonText: String,
        buttonOnPress: any
    }){
    return (
        <View>
          <Pressable style={styles.button} onPress={() => buttonOnPress}>
            <Text style={styles.buttonText}>{buttonText}</Text>
          </Pressable>
        </View>
    )
}

const styles = StyleSheet.create({
    formContainer: {
      },
      inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 56,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 12,
        marginVertical: 10,
        padding: 10,
        width: Layout.window.width * 0.8
      },
      input: {
        flex: 1,
        fontSize: 16,
        marginLeft: 10,
      },
      lupaPassword: {
        fontSize: 14,
        fontWeight: "600",
        color: "#116ACC",
        marginBottom: 30
      },

      questionContainer: {
        marginTop: 8,
        flexDirection: "row",
        justifyContent: "center"
      },
    
      qeustion: {
        textAlign: 'center',
        fontWeight: "400"
      },

      qeustionButton: {
        marginLeft: 6,
        color: "rgba(17, 106, 204, 1)",
        fontSize: 14,
        fontWeight: "600"
      },

      button: {
        alignItems: "center",
        padding: 20,
        backgroundColor: "rgba(17, 106, 204, 1)",
        borderRadius: 12
      },

      buttonText: {
        fontSize: 20,
        fontWeight: "500",
        color: "#fff"
      },

      row:{
        justifyContent: "center",
        flexDirection: "row",
      },

      collumn: {
        alignItems: "center",
        flexDirection: "column",
      }
});
