import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { Platform, StyleSheet, Button, TextInput, Dimensions, Pressable, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialIcons, Entypo } from '@expo/vector-icons';



import EditScreenInfo from '../../components/EditScreenInfo';
import { Text, View} from '../../components/Themed';
import AuthForm from './AuthComponent';
import { AuthScreenProps } from '../../types';

export default function RegisterScreen({navigation}: AuthScreenProps<'Register'>) {

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Buat Akun</Text>
      <Text style={styles.welcomeMessege}>Buat akunmu dan nikmati segala fasilitas Falet</Text>
      <AuthForm 
        isRegister={true} 
        buttonText={"Masuk"}
        buttonOnPress={()=>navigation.navigate("Login")}
        question={"Sudah punya akun?"}
        questionButton={"Masuk"}
        questionOnPress={()=>navigation.navigate("Login")}
      />
      <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 32,
    paddingStart: 32
  },
  welcomeMessege: {
    fontSize: 20,
    fontWeight: '300',
    paddingBottom: 100,
    paddingStart: 32
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  }
});
