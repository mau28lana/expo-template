import { AntDesign, Ionicons } from '@expo/vector-icons';
import { Pressable, ScrollView, StyleSheet } from 'react-native';

import EditScreenInfo from '../../components/EditScreenInfo';
import { Text, View } from '../../components/Themed';
import Layout from '../../constants/Layout';
import { RootTabScreenProps } from '../../types';
import Riwayat from './components/Riwayat';
import StatusItem from './components/StatusItem';
import StatusSurat from './components/StatusSurat';

export default function HomeScreen({ navigation }: RootTabScreenProps<'Home'>) {
  return (
    <View className='p-8'>
      <StatusSurat />

      <Riwayat />      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
    paddingTop: 32,
    paddingLeft: 24,
    paddingRight: 24,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
  },

  statusSuratContainer: {
    marginTop: 24,
    justifyContent: "center",
    flexDirection: 'row',
  },

  
});
