import React from "react";
import { StyleSheet, Pressable, Text, View} from "react-native";


export default function StatusItem({
        icon,
        text
    }: {
        icon: any,
        text: String
    }) {
    return (
        <Pressable style={styles.container}>
            {icon}
            <Text style={styles.text}>
                {text}
            </Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container:{
        marginLeft: 22,
        marginRight: 22,
    }, 
    text: {
        textAlign: "center"
    }
  });
  