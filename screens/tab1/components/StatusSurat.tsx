import { AntDesign } from "@expo/vector-icons";
import { Text, View, StyleSheet } from "react-native";
import StatusItem from "./StatusItem";



export default function StatusSurat(){
    return(
        <View>
            <Text style={styles.title}>Status surat</Text>
            <View style={styles.statusSuratContainer}>
                <StatusItem icon={<AntDesign name='edit' size={40} />} text={"Diajukan"} />
                <StatusItem icon={<AntDesign name='sync' size={40} />} text={"Diproses"} />
                <StatusItem icon={<AntDesign name='mail' size={40} />} text={"Selesai"} />
                <StatusItem icon={<AntDesign name='exclamationcircleo' size={40} />} text={"Ditolak"} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    statusSuratContainer: {
        marginTop: 24,
        justifyContent: "center",
        flexDirection: 'row',
      },    
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'left',
    },
})