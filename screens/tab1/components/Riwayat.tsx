import { Text, View } from "../../../components/Themed";
import { Pressable, StyleSheet } from 'react-native';
import { AntDesign, Entypo, Ionicons } from "@expo/vector-icons";
import Layout from "../../../constants/Layout";

export default function Riwayat(){
    return (
        <View style={styles.container}>
            <View style={styles.row}>
            <Text style={styles.title}>Riwayat</Text>
            <Pressable style={{flexDirection: "row"}}>
                <Text>Lihat Semua</Text>
                <AntDesign name="arrowright" size={14} />
            </Pressable>
            </View>
            <View style={styles.contentContainer}>
            <Pressable style={styles.card}>
                <Ionicons style={styles.iconContainer} name='document-text' size={24} color="white" />
                <View style={{flexDirection: "column", justifyContent:"space-between",marginLeft: 8}}>
                    <Text style={styles.textDate}>Tanggal</Text>
                    <Text style={styles.textTitle}>Judul</Text>
                    <Text style={styles.textType}>Jenis Surat</Text>
                </View>
                <Entypo style={{marginLeft: 150, marginTop: 12}} name="dots-three-vertical" size={24} color="gray" />
            </Pressable>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'left',
    },
    container: {
        marginTop: 32,
      },
    
      row: {
        width: Layout.window.width * 0.88,
        flexDirection: "row",
        justifyContent: "space-between",
      },
    
      contentContainer: {
        height: 300,
        borderRadius: 20,
        padding: 8,
        marginTop: 24,
        backgroundColor: "#eee"
      },
    
      card: {
        height: 90,
        borderRadius: 20,
        padding: 20,
        margin: 4,
        flexDirection: "row",
        backgroundColor: "#fff"
      },
    
      iconContainer: {
        height: 50,
        width: 50,
        borderRadius: 12,
        paddingTop: 12,
        textAlign: "center",
        backgroundColor: "red"
      },
    
      textDate: {
        fontSize: 8,
        fontWeight: "400",
        color: "rgba(104, 103, 119, 1)"
      },

      textTitle: {
        fontSize: 14,
        fontWeight: "500",
        color: "rgba(4, 2, 29, 1)"
      },

      textType: {
        fontSize: 12,
        fontWeight: "400",
        color: "rgba(104, 103, 119, 1)"
      }
});