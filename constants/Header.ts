import Layout from "./Layout"

const backgroundColor = "rgba(17, 106, 204, 1)";

export default {
    color: {
        backgroundColor: backgroundColor,
    },
    welcomeMessege: {
        fontSize: 12,
        color: "#eee",
    },
    container: {
        height: Layout.window.height * 0.2,
        backgroundColor: backgroundColor,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    }, 
    accountName: {
        fontSize: 28,
        color: "#fff"
    }
}