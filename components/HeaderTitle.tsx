import { Ionicons } from "@expo/vector-icons";
import { Text, View, TextInput } from "react-native";
import { useState } from "react";
import Colors from "../constants/Colors";
import HeaderHome from "./HeaderStyle/HeaderHome";
import {HeaderMail} from "./HeaderStyle/HeaderMail";


export default function HeaderTitle(
        {
            isHomeScreen,
            isMailScreen
        }:{
            isHomeScreen: boolean | undefined,
            isMailScreen: boolean | undefined
        }
    ){

    const [search, setSearch] = useState('');
    
    return (
        {isHomeScreen} 
        ? <View style= {Colors.backroundHeader}>
            <Text style={HeaderHome.welcomeMessege}>Selamat datang</Text>
            <Text style={[HeaderHome.accountName, {fontWeight: "600"}]}>Bunga Mawar</Text>
        </View> 
        : {isMailScreen} 
        ? <View style= {Colors.backroundHeader}>
            <View style={HeaderMail.inputContainer}>
                <Ionicons name="search-outline" size={20} color="#eee" />
                <TextInput
                    style={HeaderMail.input}
                    placeholder="Email"
                    onChangeText={text => setSearch(text)}
                    value={search}
                />
            </View>
        </View>
        : <View style= {Colors.backroundHeader}>
            <Text style={HeaderHome.welcomeMessege}>Selamat datang</Text>
            <Text style={[HeaderHome.accountName, {fontWeight: "600"}]}>Bunga Mawar</Text>
        </View>
    )
  }