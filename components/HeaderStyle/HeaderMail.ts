import { StyleSheet } from 'react-native';
import Layout from "../../constants/Layout"

export const HeaderMail = StyleSheet.create ({
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 56,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 12,
        marginVertical: 10,
        padding: 10,
        width: Layout.window.width * 0.8
    },

    input: {
        flex: 1,
        fontSize: 16,
        marginLeft: 10,
    },
})

