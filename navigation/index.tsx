/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome, Foundation, Ionicons, MaterialIcons, Octicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Pressable } from 'react-native';


import Colors from '../constants/Colors';
import Header from '../constants/Header';
import useColorScheme from '../hooks/useColorScheme';
import NotFoundScreen from '../screens/NotFoundScreen';
import HomeScreen from '../screens/tab1/HomeScreen';
import MailScreen from '../screens/tab2/MailScreen';
import { AuthParamList, RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import LoginScreen from '../screens/auth/LoginScreen';
import RegisterScreen from '../screens/auth/RegisterScreen';
import { Text, View } from '../components/Themed';
import HeaderTitle from '../components/HeaderTitle';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      <Stack.Screen name='Auth' component={AuthNavigator} options={{headerShown: false}} />
    </Stack.Navigator>
  );
}

const Auth = createNativeStackNavigator<AuthParamList>();

function AuthNavigator() {
  return (
    <Auth.Navigator 
      initialRouteName='Login'
    >
      <Auth.Group screenOptions={{presentation: 'modal'}}>
        <Auth.Screen name="Login" component={LoginScreen} />
        <Auth.Screen name="Register" component={RegisterScreen} />
      </Auth.Group>
    </Auth.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={({ navigation }: RootTabScreenProps<'Home'>) => ({
          title: 'Beranda',
          tabBarIcon: ({ color }) => <Ionicons name="home-outline" size={28} color={color} />,
          headerTitle: () => (
            <HeaderTitle isHomeScreen={true} isMailScreen={false} />
          ),
          headerTintColor: "white",
          headerStyle: Header.container,
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('Auth')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <Ionicons
                name="notifications-outline"
                size={28}
                color={"white"}
                style={{ marginRight: 15 }}
              />
            </Pressable>
          ),
        })}
      />
      <BottomTab.Screen
         name="Mail"
         component={MailScreen}
         options={({ navigation }: RootTabScreenProps<'Mail'>) => ({
           title: 'Surat',
           tabBarIcon: ({ color }) => <Ionicons name="mail-outline" size={28} color={color} />,
           headerTitle: () => (
             <HeaderTitle isHomeScreen={false} isMailScreen={true} />
           ),
           headerTintColor: "white",
           headerStyle: Header.container,
           headerRight: () => (
            <View style={{flexDirection: "row"}}>
              <Pressable
                onPress={() => navigation.navigate('Auth')}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                })}>
                <Ionicons
                  name="notifications-outline"
                  size={28}
                  color={"white"}
                  style={{ marginRight: 15 }}
                />
              </Pressable>
              <Pressable
                onPress={() => navigation.navigate('Auth')}
                style={({ pressed }) => ({
                  opacity: pressed ? 0.5 : 1,
                })}>
                <Ionicons
                  name="notifications-outline"
                  size={28}
                  color={"white"}
                  style={{ marginRight: 15 }}
                />
              </Pressable>
            </View>
             
           ),
         })}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>['name'];
  color: string;
}) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}


