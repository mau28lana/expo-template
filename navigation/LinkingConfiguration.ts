/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';

import { RootStackParamList } from '../types';

const linking: LinkingOptions<RootStackParamList> = {
  prefixes: [Linking.createURL('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Home: {
            screens: {
              HomeScreen: 'tab1',
            },
          },
          Mail: {
            screens: {
              MailScreen: 'tab2',
            },
          },
          Chat: {
            screens: {
              ChatScreen: 'tab3',
            },
          },
          Account: {
            screens: {
              AccountScreen: "tab4"
            },
          },
        },
      },
      Auth: {
        screens: {
          Login: {
            screens: {
              LoginScreen: 'login_screen',
            }
          },
          Register: {
            screens: {
              RegisterScreen: 'register_screen',
            }
          }
        }
      },
      // Login: 'login',
      // Register: 'register',
      NotFound: '*',
    },
  },
};

export default linking;
